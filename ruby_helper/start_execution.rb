#!/usr/bin/env rvm-auto-ruby

require 'rubygems'
require 'bundler/setup'
require 'optparse'
require 'gooddata'

require_relative 'ruby_helper_config'

helper_config = Ruby_helper_config.new

options = {}
options[:username] = helper_config.username
options[:password] = helper_config.password
options[:server]   = helper_config.server || nil

OptionParser.new do |opts|
  opts.on('-p', '--project PID',  'Project ID' ) { |p| options[:project]  = p }
  opts.on('-s', '--schedule SID', 'Schedule ID') { |s| options[:schedule] = s }
end.parse!

raise OptionParser::MissingArgument if options[:project].nil? or options[:schedule].nil?

module GoodData
  class Schedule
    def execute_no_wait
      data = { :execution => {} }
      execution = client.post(execution_url, data, :headers => {'X-GDC-CC-PRIORITY-MODE' => 'NORMAL'})
      res = client.poll_on_response(execution['execution']['links']['self']) do |body|
        body['execution'] && ! (body['execution']['status'] == 'RUNNING' || body['execution']['status'] == 'SCHEDULED')
      end
      client.create(GoodData::Execution, res, client: client, project: project)
    end
  end
end

if options[:server]
	client = GoodData.connect(options[:username], options[:password], :server => options[:server])
else
	client = GoodData.connect(options[:username], options[:password])
end

begin
	project = client.projects(options[:project])
	schedule = project.schedules(options[:schedule])
  schedule.execute_no_wait
rescue
	puts 'Something went wrong'
	client.disconnect
  exit(1)
end

exit(0)
