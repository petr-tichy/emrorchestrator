#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2007-2013, GoodData(R) Corporation. All rights reserved

__author__ = 'Petr Tichý <petr.tichy@gooddata.com>'

import sys
import time
from datetime import date
import logging
import argparse
import csv

sys.path.insert(0, './boto')

input_bucket_name = 'gdc-ms-cust'
output_bucket_name = 'gdc-ms-emr'

logpath = 's3://gdc-ms-emr/gdc-ms-emr_Sailthru/emr_logs/'

from os.path import dirname
from inspect import getsourcefile

SCRIPT_HOME = dirname(getsourcefile(lambda _: None))
if SCRIPT_HOME == '':
    SCRIPT_HOME = '.'
RUBY_HELPER = 'timeout 60 ./start_execution.rb'


def data_not_available(final, customer_list, group):
    from email.mime.text import MIMEText
    from subprocess import Popen, PIPE

    if debug:
        email = ['petr.tichy@gooddata.com']
    else:
        email = ['ms+pagerduty@gooddata.com', 'analytics@sailthru.com', 'jfarris@sailthru.com']

    if final:
        msg = MIMEText('\n'.join([
            'The EMR process for Sailthru customer IDs {0} in group {1} was not started'.format(
                ', '.join(customer_list), group),
            'because the JSON data were not available before {0}'.format(
                time.strftime("%Y-%m-%d %H:%M:%S UTC", time.gmtime())),
            'Waited for {0} minutes'.format(retry * sleep / 60)]))
        msg['Subject'] = 'Sailthru EMR ERROR: group {0} missing customers data!'.format(group)
    else:
        msg = 'Data for customer ID(s) {0} in group {1} are not available on time. Retrying for {2} minutes.'
        msg = MIMEText(msg.format(', '.join(customer_list), group, retry * sleep / 60))
        msg['Subject'] = 'Sailthru EMR warning: data not available on time'

    cmd = ["/usr/sbin/sendmail", "-oi"]
    cmd.extend(email)
    p = Popen(cmd, stdin=PIPE)
    p.communicate(msg.as_string())

    # print('enough waiting for customer(s) {0}'.format(', '.join(customer_list)))


def load_cps_map():
    cps_map = dict()
    with open(SCRIPT_HOME + '/sailthru-cps-map.csv', 'rb') as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            cps_map.update({row[0]: (row[1], row[2])})
    return cps_map


class S3fileLister:
    check_list = [
        'client_horizon.json',
        'profile.json',
        'stats_blast.json',
        'stats_pv_hour.json',
        'stats_recommend_day.json',
        'stats_scout_day.json',
        'stats_send.json',
        'purchase.json',
        'label.json',
        'blast.json'
    ]

    def __init__(self, bucket, prefix):
        self.log = logging.getLogger('sailthru-emr.S3fileLister')
        self.bucket = bucket
        self.prefix = prefix
        self.check = set([self.prefix + i for i in self.check_list])

    def iscomplete(self):
        filelist = list(self.bucket.list(self.prefix, '/'))
        uploaded = set([i.name for i in filelist])
        if uploaded >= self.check:
            self.log.info('{0}: data OK'.format(self.prefix))
            return True
        else:
            self.log.info('{0}: data NAK'.format(self.prefix))
            self.log.info('Missing: {0}'.format(self.check - uploaded))
            return False

    def fixup(self):
        from boto.s3.key import Key

        filelist = self.bucket.list(self.prefix, '/')
        for i in filelist:
            if i.name in self.check and i.size == 0 and i.etag == '"d41d8cd98f00b204e9800998ecf8427e"':
                if dry_run:
                    self.log.info('Would fix {0}'.format(i.name))
                else:
                    self.log.info('Fixing {0}'.format(i.name))
                    k = Key(self.bucket, i.name)
                    k.set_contents_from_string('{}')


class Customer:
    def __init__(self, customerid, datepath, bucket):
        self.customerid = customerid
        self.datepath = datepath
        self.attempts_left = retry
        self.state = 0
        self.sourcepath = 'AIDAI6WC3D3RBA2KCEQSO_gdc-ms-cust_Sailthru/' + self.datepath + '/' + self.customerid + '/'
        self.sourcepathuri = 's3://' + input_bucket_name + '/' + self.sourcepath
        self.log = logging.getLogger('sailthru-emr.Customer')
        self.file_check = S3fileLister(bucket, self.sourcepath)

    def run_emr(self):
        if self.state == 2:  # 2 - no data
            return True
        elif self.state == 1:  # 1 - data ready
            return True
        elif self.state == 0:  # 0 - new
            if self.file_check.iscomplete():
                self.state = 1
                return True
            elif self.attempts_left > 0:
                self.attempts_left -= 1
                self.log.info('waiting for {0}'.format(self.customerid))
                if debug:  # TODO: remove
                    self.state = 1
                return False
            else:
                self.state = 2
                self.log.info('enough waiting for {0}'.format(self.customerid))
                return True
        else:
            raise AssertionError('unknown state {0}'.format(self.state))

    def fix_files(self):
        if self.state == 1:  # data ready
            self.file_check.fixup()


class EMRconn:
    def __init__(self, conn, datepath, suffix, cluster_type):
        self.conn = conn
        self.datepath = datepath
        self.suffix = suffix if suffix else ''
        self.jobflow_id = None
        self.dstpath = 'gdc-ms-emr_Sailthru/output/'
        self.dstpath += self.datepath + '/' + self.suffix + '/'
        self.resultpathuri = 's3://' + output_bucket_name + '/' + self.dstpath
        self.action_on_failure = 'TERMINATE_JOB_FLOW'  # 'CANCEL_AND_WAIT'
        self.keep_alive = False  # True
        self.log = logging.getLogger('sailthru-emr.EMRconn')
        self.cluster_type = cluster_type

    @staticmethod
    def bootstrap_steps(cluster_type):
        from boto.emr.bootstrap_action import BootstrapAction

        bootstrap_steps = []
        configure_hadoop_params = []

        # configure number of tasks for cluster instance type
        if cluster_type == 0:
            configure_hadoop_params.extend(['-m', 'mapred.tasktracker.map.tasks.maximum=32',
                                            '-m', 'mapred.tasktracker.reduce.tasks.maximum=0'])

        # enable output compression
        configure_hadoop_params.extend([
            '-m', 'mapred.output.compress=true',
            '-m', 'mapred.output.compression.type=BLOCK',
            '-m', 'mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec'
        ])
        # configure_hadoop_params.extend(['-m', 'mapred.task.timeout=2400000'])
        if configure_hadoop_params:
            bootstrap_steps.append(
                BootstrapAction('ConfigureCluster', 's3://elasticmapreduce/bootstrap-actions/configure-hadoop',
                                configure_hadoop_params))

        # install Ganglia for debug
        if debug:
            bootstrap_steps.append(
                BootstrapAction('InstallGanglia', 's3://elasticmapreduce/bootstrap-actions/install-ganglia', []))

        return bootstrap_steps

    @staticmethod
    def instance_groups(cluster_type):
        from boto.emr.instance_group import InstanceGroup

        if cluster_type == 3:
            return [InstanceGroup(1, 'MASTER', 'm1.large', 'ON_DEMAND', 'gdc-ms-sailthru-master'),
                    InstanceGroup(2, 'CORE', 'cc2.8xlarge', 'ON_DEMAND', 'gdc-ms-sailthru-core'),
                    InstanceGroup(6, 'TASK', 'cc2.8xlarge', 'SPOT', 'gdc-ms-sailthru-task', '1.0')]
        elif cluster_type == 2:
            return [InstanceGroup(1, 'MASTER', 'm1.medium', 'ON_DEMAND', 'gdc-ms-sailthru-master'),
                    InstanceGroup(4, 'CORE', 'c3.xlarge', 'ON_DEMAND', 'gdc-ms-sailthru-core')]
        elif cluster_type == 1:
            return [InstanceGroup(1, 'MASTER', 'm1.medium', 'ON_DEMAND', 'gdc-ms-sailthru-master'),
                    InstanceGroup(4, 'CORE', 'c3.4xlarge', 'ON_DEMAND', 'gdc-ms-sailthru-core')]
        elif cluster_type == 0:
            return [InstanceGroup(1, 'MASTER', 'm1.medium', 'ON_DEMAND', 'gdc-ms-sailthru-master'),
                    InstanceGroup(2, 'CORE', 'c3.8xlarge', 'ON_DEMAND', 'gdc-ms-sailthru-core')]
        else:
            assert "Invalid cluster_type"

    def jarstep(self, sourcepaths):
        from boto.emr.step import JarStep

        return JarStep(name='sailthru-emr-etl',
                       jar='s3://gdc-ms-emr/gdc-ms-emr_Sailthru/jar/parkour_play-1.1.7-IAM-Roles-SNAPSHOT-standalone.jar',
                       main_class='dummy',
                       step_args=[self.resultpathuri] + sourcepaths,
                       action_on_failure=self.action_on_failure)

    def run_jobflow(self, customers):
        sourcepaths = [i.sourcepathuri for i in customers]
        jarsteps = [self.jarstep(sourcepaths)]
        if dry_run:
            self.log.info('Would run jar with paths {0}'.format(', '.join(sourcepaths)))
            self.jobflow_id = 'j-3HXY899HK63VA'
        else:
            self.log.info('Running jar with paths {0}'.format(', '.join(sourcepaths)))
            self.jobflow_id = self.conn.run_jobflow(name='sailthru-emr-cron-' + self.suffix,
                                                    log_uri=logpath,
                                                    steps=jarsteps,
                                                    ami_version='2.4.11',
                                                    service_role='EMR_DefaultRole',
                                                    job_flow_role='EMR_EC2_DefaultRole',
                                                    # ec2_keyname='emr-sailthru',
                                                    instance_groups=self.instance_groups(self.cluster_type),
                                                    visible_to_all_users=True,
                                                    enable_debugging=False,
                                                    availability_zone='us-east-1b',
                                                    keep_alive=self.keep_alive,
                                                    bootstrap_actions=self.bootstrap_steps(self.cluster_type),
                                                    action_on_failure=self.action_on_failure)
        return self.jobflow_id

    def jobflow_done(self):
        if dry_run:
            return True
        else:
            return hasattr(self.conn.describe_jobflow(jobflow_id=self.jobflow_id), 'enddatetime')

    def jobflow_ok(self):
        if dry_run:
            return True
        else:
            return self.conn.describe_jobflow(jobflow_id=self.jobflow_id).state == 'COMPLETED'


def all_ready(customers):
    done = True
    for i in customers:
        done &= i.run_emr()
    if done:
        for i in customers:
            i.fix_files()
    return done


def start_etl(customers_to_run):
    """ Start the GoodData ETL """

    import subprocess

    cps_map = load_cps_map()

    for customer in customers_to_run:
        try:
            (pid, sid) = cps_map[customer]
        except KeyError:
            logging.error("ETL for ClientID {0} not found in config".format(customer))
            continue
        helper_command = " ".join([RUBY_HELPER, '-p', pid, '-s', sid])
        cmd = ['bash', '-cl', helper_command]
        if dry_run:
            logging.info("Would invoke helper command: {0}".format(" ".join(cmd)))
        else:
            p = subprocess.Popen(cmd, cwd=SCRIPT_HOME + '/ruby_helper')
            p.wait()
            if p.returncode == 0:
                logging.info("ETL started for CID {0} PID {1} SID {2}".format(customer, pid, sid))
            else:
                logging.error("FAILED to start ETL for CID {0} PID {1} SID {2}".format(customer, pid, sid))


def main():
    global dry_run, debug, retry, sleep
    from boto.emr.connection import EmrConnection
    from boto.s3.connection import S3Connection

    parser = argparse.ArgumentParser()
    parser.add_argument('-c', dest='customer', nargs='+', required=True)
    parser.add_argument('-d', '--date', default=date.today().strftime('%Y%m%d'), dest='datepath', metavar='DATE')
    parser.add_argument('-s', '--suffix', default=None)
    parser.add_argument('-r', '--retry', default=24, type=int, dest='retry')
    parser.add_argument('-t', '--sleep-seconds', default=300, type=int, dest='sleep')
    parser.add_argument('-e', '--run-etl', default=False, dest='run_etl', action='store_true')
    parser.add_argument('-l', '--cluster-type', default=0, type=int, dest='cluster_type')
    parser.add_argument('-n', '--dry-run', default=False, dest='dry_run', action='store_true')
    parser.add_argument('-v', '--verbose', default=0, dest='verbose', action='count')
    args = parser.parse_args()

    debug = False
    if args.verbose > 1:
        logging.basicConfig(level=logging.DEBUG)
        debug = True
    elif args.verbose > 0:
        logging.basicConfig(level=logging.INFO)
    else:
        logging.basicConfig(level=logging.WARNING)

    dry_run = args.dry_run
    retry = args.retry
    sleep = args.sleep

    log = logging.getLogger('sailthru-emr')
    log.info('start')

    s3conn = S3Connection()
    bucket = s3conn.get_bucket(bucket_name=input_bucket_name, validate=False)

    customers = [Customer(customerid=str(i), datepath=args.datepath, bucket=bucket) for i in args.customer]

    customers_first_run = True
    while not all_ready(customers):
        if customers_first_run:
            customers_first_run = False
            failed_list = [i.customerid for i in customers if i.state != 1]
            if failed_list:
                data_not_available(False, failed_list, args.suffix)
        log.info('sleeping')
        time.sleep(sleep)

    customers_to_run = [i for i in customers if i.state == 1]
    if customers_to_run:
        emr = EMRconn(conn=EmrConnection(), datepath=args.datepath,
                      suffix=args.suffix, cluster_type=args.cluster_type)
        jobflowid = emr.run_jobflow(customers_to_run)
        if args.run_etl:
            emr_retry = 120
            while emr_retry > 0:
                if emr.jobflow_done():
                    break
                else:
                    emr_retry -= 1
                    time.sleep(60)
            if emr.jobflow_ok():
                start_etl([i.customerid for i in customers_to_run])
            else:
                log.error("EMR JobFlow {0} failed".format(jobflowid))

    failed_list = [i.customerid for i in customers if i.state != 1]
    if failed_list:
        data_not_available(True, failed_list, args.suffix)


if __name__ == '__main__':
    main()

# vim: ts=4 et sw=4 sts=4
